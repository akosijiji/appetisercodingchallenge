package com.msaysabio.appetisercodingchallenge.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.msaysabio.appetisercodingchallenge.R;
import com.msaysabio.appetisercodingchallenge.databinding.MovieDetailActivityBinding;
import com.msaysabio.appetisercodingchallenge.model.Movie;
import com.msaysabio.appetisercodingchallenge.viewmodel.MovieDetailViewModel;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

/**
 * Created by Mary.Sabio on 30-08-2019.
 */
public class MovieDetailActivity extends AppCompatActivity {
	
	private static final String EXTRA_MOVIE = "EXTRA_MOVIE";
	
	private MovieDetailActivityBinding movieDetailActivityBinding;
	
	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		movieDetailActivityBinding =
				DataBindingUtil.setContentView(this, R.layout.movie_detail_activity);
		setSupportActionBar(movieDetailActivityBinding.toolbar);
		displayHomeAsUpEnabled();
		getExtrasFromIntent();
	}
	
	public static Intent launchDetail(Context context, Movie movie) {
		Intent intent = new Intent(context, MovieDetailActivity.class);
		intent.putExtra(EXTRA_MOVIE, movie);
		return intent;
	}
	
	private void displayHomeAsUpEnabled() {
		ActionBar actionBar = getSupportActionBar();
		if (actionBar != null) {
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
	}
	
	private void getExtrasFromIntent() {
		Movie movie = (Movie) getIntent().getSerializableExtra(EXTRA_MOVIE);
		MovieDetailViewModel movieDetailViewModel = new MovieDetailViewModel(movie);
		movieDetailActivityBinding.setMovieDetailViewModel(movieDetailViewModel);
		setTitle(movie.trackName);
	}
	
}
