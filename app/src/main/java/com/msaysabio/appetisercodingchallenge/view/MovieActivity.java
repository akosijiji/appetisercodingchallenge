package com.msaysabio.appetisercodingchallenge.view;

import android.os.Bundle;

import com.msaysabio.appetisercodingchallenge.R;
import com.msaysabio.appetisercodingchallenge.databinding.MovieActivityBinding;
import com.msaysabio.appetisercodingchallenge.viewmodel.MovieViewModel;

import java.util.Observable;
import java.util.Observer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


/**
 * Created by Mary.Sabio on 30-08-2019.
 */
public class MovieActivity extends AppCompatActivity implements Observer {
	
	private MovieActivityBinding movieActivityBinding;
	private MovieViewModel movieViewModel;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		initDataBinding();
		setSupportActionBar(movieActivityBinding.toolbar);
		setupListMovieView(movieActivityBinding.listMovie);
		setupObserver(movieViewModel);
	}
	
	private void initDataBinding() {
		movieActivityBinding = DataBindingUtil.setContentView(this, R.layout.movie_activity);
		movieViewModel = new MovieViewModel(this);
		movieActivityBinding.setMainViewModel(movieViewModel);
	}
	
	private void setupListMovieView(RecyclerView listMovie) {
		MovieAdapter adapter = new MovieAdapter();
		listMovie.setAdapter(adapter);
		listMovie.setLayoutManager(new LinearLayoutManager(this));
		
	}
	
	public void setupObserver(Observable observable) {
		observable.addObserver(this);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		movieViewModel.reset();
	}
	
	@Override
	public void update(Observable observable, Object data) {
		if (observable instanceof MovieViewModel) {
			MovieAdapter movieAdapter = (MovieAdapter) movieActivityBinding.listMovie.getAdapter();
			MovieViewModel movieViewModel = (MovieViewModel) observable;
			movieAdapter.setMovieList(movieViewModel.getMovieList());
		}
	}
	
}