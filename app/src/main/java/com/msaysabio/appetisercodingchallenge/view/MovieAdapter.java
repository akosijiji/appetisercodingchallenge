package com.msaysabio.appetisercodingchallenge.view;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.msaysabio.appetisercodingchallenge.R;
import com.msaysabio.appetisercodingchallenge.databinding.ItemMovieBinding;
import com.msaysabio.appetisercodingchallenge.model.Movie;
import com.msaysabio.appetisercodingchallenge.viewmodel.ItemMovieViewModel;

import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Mary.Sabio on 30-08-2019.
 */
public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieAdapterViewHolder> {
	
	private List<Movie> peopleList;
	
	MovieAdapter() {
		this.peopleList = Collections.emptyList();
	}
	
	@NonNull
	@Override public MovieAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		ItemMovieBinding itemMovieBinding =
				DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_movie,
				                        parent, false);
		return new MovieAdapterViewHolder(itemMovieBinding);
	}
	
	@Override public void onBindViewHolder(MovieAdapterViewHolder holder, int position) {
		holder.bindMovie(peopleList.get(position));
	}
	
	@Override public int getItemCount() {
		return peopleList.size();
	}
	
	void setMovieList(List<Movie> peopleList) {
		this.peopleList = peopleList;
		notifyDataSetChanged();
	}
	
	static class MovieAdapterViewHolder extends RecyclerView.ViewHolder {
		ItemMovieBinding mItemMovieBinding;
		
		MovieAdapterViewHolder(ItemMovieBinding itemMovieBinding) {
			super(itemMovieBinding.itemMovie);
			this.mItemMovieBinding = itemMovieBinding;
		}
		
		void bindMovie(Movie people) {
			if (mItemMovieBinding.getMovieViewModel() == null) {
				mItemMovieBinding.setMovieViewModel(
						new ItemMovieViewModel(people, itemView.getContext()));
			} else {
				mItemMovieBinding.getMovieViewModel().setMovie(people);
			}
		}
	}
}
