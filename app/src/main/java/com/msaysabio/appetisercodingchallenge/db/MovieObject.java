package com.msaysabio.appetisercodingchallenge.db;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Created by Mary.Sabio on 30-08-2019.
 */
@Entity(tableName = "movie")
public class MovieObject {
	
	@PrimaryKey
	@NonNull
	@SerializedName("id")
	@Expose
	private String id;
	
	@SerializedName("trackName")
	@Expose
	public String trackName;
	
	@SerializedName("collectionName")
	@Expose
	public String collectionName;
	
	@SerializedName("artistName")
	@Expose
	public String artistName;
	
	@SerializedName("artworkUrl100")
	@Expose
	public String artworkUrl100;
	
	@SerializedName("country")
	@Expose
	public String country;
	
	@SerializedName("currency")
	@Expose
	public String currency;
	
	@SerializedName("primaryGenreName")
	@Expose
	public String primaryGenreName;
	
	@SerializedName("collectionPrice")
	@Expose
	public String collectionPrice;
	
	@SerializedName("trackPrice")
	@Expose
	public String trackPrice;
	
	@SerializedName("trackRentalPrice")
	@Expose
	public String trackRentalPrice;
	
	@SerializedName("releaseDate")
	@Expose
	public String releaseDate;
	
	@SerializedName("longDescription")
	@Expose
	public String longDescription;
	
	@SerializedName("previewUrl")
	@Expose
	public String previewUrl;
	
	private Date lastRefresh;
	
	// --- CONSTRUCTORS ---
	
	public MovieObject() { }
	
	public MovieObject(@NonNull String id, String trackName, String collectionName, String artistName,
	                   String artworkUrl100, String primaryGenreName, String trackPrice, String longDescription, Date lastRefresh) {
		this.id = id;
		this.trackName = trackName;
		this.collectionName = collectionName;
		this.artistName = artistName;
		this.artworkUrl100 = artworkUrl100;
		this.primaryGenreName = primaryGenreName;
		this.trackPrice = trackPrice;
		this.longDescription = longDescription;
		this.lastRefresh = lastRefresh;
	}
	
	public MovieObject(String artistName, String trackName) {
		this.artistName = artistName;
		this.trackName = trackName;
	}
	
	// --- GETTER ---
	
	public String getId() { return id; }
	public String getCollectionName() { return collectionName; }
	public Date getLastRefresh() { return lastRefresh; }
	public String getTrackName() { return trackName; }
	public String getArtistName() { return artistName; }
	public String getArtworkUrl100() { return artworkUrl100; }
	public String getPrimaryGenreName() { return primaryGenreName; }
	public String getTrackPrice() { return trackPrice; }
	public String getLongDescription() { return longDescription; }
	
	
	// --- SETTER ---
	
	public void setId(String id) { this.id = id; }
	public void setCollectionName(String collectionName) { this.collectionName = collectionName; }
	public void setLastRefresh(Date lastRefresh) { this.lastRefresh = lastRefresh; }
	public void setTrackName(String trackName) { this.trackName = trackName; }
	public void setArtistName(String artistName) { this.artistName = artistName; }
	public void setArtworkUrl100(String artworkUrl100) { this.artworkUrl100 = artworkUrl100; }
	public void setPrimaryGenreName(String primaryGenreName) { this.primaryGenreName = primaryGenreName; }
	public void setTrackPrice(String trackPrice) { this.trackPrice = trackPrice; }
	public void setLongDescription(String longDescription) { this.longDescription = longDescription; }

}
