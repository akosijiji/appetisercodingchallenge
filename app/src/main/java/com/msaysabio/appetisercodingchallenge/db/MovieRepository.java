package com.msaysabio.appetisercodingchallenge.db;

import android.app.Application;
import android.os.AsyncTask;

import java.util.List;

import androidx.lifecycle.LiveData;

/**
 * Created by Mary.Sabio on 30-08-2019.
 */
//@Singleton
public class MovieRepository {
	
	private MovieDao mMovieDao;
	private LiveData<List<MovieObject>> mAllMovies;
	
	// Note that in order to unit test the MovieRepository, you have to remove the Application
	// dependency. This adds complexity and much more code, and this sample is not about testing.
	// See the BasicSample in the android-architecture-components repository at
	// https://github.com/googlesamples
	public MovieRepository(Application application) {
		MyDatabase db = MyDatabase.getDatabase(application);
		mMovieDao = db.movieDao();
		mAllMovies = mMovieDao.getMovies();
	}
	
	// Room executes all queries on a separate thread.
	// Observed LiveData will notify the observer when the data has changed.
	public LiveData<List<MovieObject>> getAllMovies() {
		return mAllMovies;
	}
	
	// You must call this on a non-UI thread or your app will crash.
	// Like this, Room ensures that you're not doing any long running operations on the main
	// thread, blocking the UI.
	public void insert(MovieObject word) {
		new insertAsyncTask(mMovieDao).execute(word);
	}
	
	private static class insertAsyncTask extends AsyncTask<MovieObject, Void, Void> {
		
		private MovieDao mAsyncTaskDao;
		
		insertAsyncTask(MovieDao dao) {
			mAsyncTaskDao = dao;
		}
		
		@Override
		protected Void doInBackground(final MovieObject... params) {
			mAsyncTaskDao.save(params[0]);
					//.insert(params[0]);
			return null;
		}
	}
	
//	private static int FRESH_TIMEOUT_IN_MINUTES = 3;
	
//	private final UserWebservice webservice;
//	private final MovieDao userDao;
//	private final Executor executor;
	
//	@Inject
//	public MovieRepository(UserWebservice webservice, MovieDao userDao, Executor executor) {
//		this.webservice = webservice;
//		this.userDao = userDao;
//		this.executor = executor;
//	}
//
//	// ---
//
//	public LiveData<Movie> getMovie(String userLogin) {
//		refreshUser(userLogin); // try to refresh data if possible from Github Api
//		return userDao.load(userLogin); // return a LiveData directly from the database.
//	}
//
//	// ---
//
//	private void refreshUser(final String userLogin) {
//		executor.execute(() -> {
//			// Check if user was fetched recently
//			boolean userExists = (userDao.hasMovie(userLogin, getMaxRefreshTime(new Date())) != null);
//			// If user have to be updated
//			if (!userExists) {
//				webservice.getMovie(userLogin).enqueue(new Callback<Movie>() {
//					@Override
//					public void onResponse(Call<Movie> call, Response<Movie> response) {
//						Toast.makeText(App.context, "Data refreshed from network !", Toast.LENGTH_LONG).show();
//						executor.execute(() -> {
//							Movie user = response.body();
//							user.setLastRefresh(new Date());
//							userDao.save(user);
//						});
//					}
//
//					@Override
//					public void onFailure(Call<User> call, Throwable t) { }
//				});
//			}
//		});
//	}
//
//	// ---
//
//	private Date getMaxRefreshTime(Date currentDate){
//		Calendar cal = Calendar.getInstance();
//		cal.setTime(currentDate);
//		cal.add(Calendar.MINUTE, -FRESH_TIMEOUT_IN_MINUTES);
//		return cal.getTime();
//	}
	
}
