package com.msaysabio.appetisercodingchallenge.db;

import java.util.Date;

import androidx.room.TypeConverter;

/**
 * Created by Mary.Sabio on 30-08-2019.
 */
public class DateConverter {
	
	@TypeConverter
	public static Date toDate(Long timestamp) {
		return timestamp == null ? null : new Date(timestamp);
	}
	
	@TypeConverter
	public static Long toTimestamp(Date date) {
		return date == null ? null : date.getTime();
	}
	
}
