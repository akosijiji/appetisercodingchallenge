package com.msaysabio.appetisercodingchallenge.db;

import java.util.Date;
import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

/**
 * Created by Mary.Sabio on 30-08-2019.
 */
@Dao
public interface MovieDao {
	
	@Insert(onConflict = REPLACE)
	void save(MovieObject movie);
	
	@Query("SELECT * FROM movie WHERE trackName = :movieTrackName")
	LiveData<MovieObject> load(String movieTrackName);
	
	@Query("SELECT * FROM movie")
	LiveData<List<MovieObject>> getMovies();
	
	@Query("SELECT * FROM movie WHERE trackName = :movieTrackName AND lastRefresh > :lastRefreshMax LIMIT 1")
	MovieObject hasMovie(String movieTrackName, Date lastRefreshMax);

}
