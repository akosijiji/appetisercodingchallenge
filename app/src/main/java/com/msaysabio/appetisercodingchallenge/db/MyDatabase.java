package com.msaysabio.appetisercodingchallenge.db;

import android.content.Context;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

/**
 * Created by Mary.Sabio on 30-08-2019.
 */
@Database(entities = {MovieObject.class}, version = 1, exportSchema = false)
@TypeConverters(DateConverter.class)
public abstract class MyDatabase extends RoomDatabase {
	
	// --- SINGLETON ---
	private static volatile MyDatabase INSTANCE;
	
	// --- DAO ---
	public abstract MovieDao movieDao();
	
	static MyDatabase getDatabase(final Context context) {
		if (INSTANCE == null) {
			synchronized (MyDatabase.class) {
				if (INSTANCE == null) {
					INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
					                                MyDatabase.class, "movie_database")
					               // Wipes and rebuilds instead of migrating if no Migration object.
					               // Migration is not part of this codelab.
					               .fallbackToDestructiveMigration()
					               .addCallback(sRoomDatabaseCallback)
					               .build();
				}
			}
		}
		return INSTANCE;
	}
	
	/**
	 * Override the onOpen method to populate the database.
	 * For this sample, we clear the database every time it is created or opened.
	 */
	private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback(){
		
		@Override
		public void onOpen(@NonNull SupportSQLiteDatabase db){
			super.onOpen(db);
			// If you want to keep the data through app restarts,
			// comment out the following line.
			new PopulateDbAsync(INSTANCE).execute();
		}
	};
	
	/**
	 * Populate the database in the background.
	 * If you want to start with more words, just add them.
	 */
	private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {
		
		private final MovieDao mDao;

		PopulateDbAsync(MyDatabase db) {
			mDao = db.movieDao();
		}
		
		@Override
		protected Void doInBackground(final Void... params) {
			// Start the app with a clean database every time.
			// Not needed if you only populate on creation.
			//			mDao.deleteAll();
			List<MovieObject> movies = new ArrayList<>();
			MovieObject movie = new MovieObject();
			movie.setArtistName("J.J. Abrams");
			movie.setTrackName("Star Trek Into Darkness");
			movies.add(movie);
			
			for( int i = 0; i <= movies.size() - 1; i++) {
				MovieObject word = new MovieObject(movies.get(i).artistName, movies.get(i).getTrackName());
				mDao.save(word);
			}
			return null;
		}
	}
	
}
