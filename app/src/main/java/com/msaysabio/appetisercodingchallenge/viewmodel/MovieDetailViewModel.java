package com.msaysabio.appetisercodingchallenge.viewmodel;

import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.msaysabio.appetisercodingchallenge.R;
import com.msaysabio.appetisercodingchallenge.model.Movie;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;

/**
 * Created by Mary.Sabio on 30-08-2019.
 */
public class MovieDetailViewModel {
	
	private Movie movie;

	public MovieDetailViewModel(Movie movie) {
		this.movie = movie;
	}
	
	public String getTrackName() {
		return movie.trackName;
	}
	
	public String getReleaseDate() {
		return movie.releaseDate;
	}
	
	public String getArtistName() {
		return movie.artistName;
	}
	
	public String getCollectionName() {
		return movie.collectionName;
	}
	
	public String getPreviewUrl() {
		return movie.previewUrl;
	}
	
	public String getCurrency() {
		return movie.currency;
	}
	
	public String getCountry() {
		return movie.country;
	}
	
	public String getPictureProfile() {
		return movie.artworkUrl100;
	}
	
	public String getCollectionPrice() {
		return movie.collectionPrice;
	}
	
	public String getTrackPrice() {
		return movie.trackPrice;
	}
	
	public String getTrackRentalPrice() {
		return movie.trackRentalPrice;
	}
	
	public String getPrimaryGenreName() {
		return movie.primaryGenreName;
	}
	
	public String getLongDescription() {
		return movie.longDescription;
	}
	
	@BindingAdapter({"imageUrl"})
	public static void loadImage(ImageView view, String imageUrl) {
		Glide.with(view.getContext())
		     .load(imageUrl)
		     .placeholder(R.drawable.ic_placeholder)
		     .into(view);
	}
	
	@BindingAdapter(("releaseDate"))
	public static void fortmatDate(@NonNull TextView textView, String releaseDate) {
		SimpleDateFormat fromServer = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat myFormat = new SimpleDateFormat("MMM dd, yyyy");
		Date date = null;
		try {
			date = fromServer.parse(releaseDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		String outputDate = myFormat.format(date);
		textView.setText(outputDate);
	}
	
}