package com.msaysabio.appetisercodingchallenge.viewmodel;

import android.app.Application;
import android.content.Context;
import android.view.View;

import com.msaysabio.appetisercodingchallenge.AppetiserApplication;
import com.msaysabio.appetisercodingchallenge.R;
import com.msaysabio.appetisercodingchallenge.data.AppetiserFactory;
import com.msaysabio.appetisercodingchallenge.data.AppetiserResponse;
import com.msaysabio.appetisercodingchallenge.data.AppetiserService;
import com.msaysabio.appetisercodingchallenge.db.MovieObject;
import com.msaysabio.appetisercodingchallenge.db.MovieRepository;
import com.msaysabio.appetisercodingchallenge.model.Movie;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;
import androidx.lifecycle.LiveData;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;


/**
 * Created by Mary.Sabio on 30-08-2019.
 */
public class MovieViewModel extends Observable {
	
	public ObservableInt movieProgress;
	public ObservableInt movieRecycler;
	public ObservableInt movieLabel;
	public ObservableField<String> messageLabel;
	
	private List<Movie> movieList;
	private Context context;
	private CompositeDisposable compositeDisposable = new CompositeDisposable();
	
	private MovieRepository mRepository;
	private LiveData<List<MovieObject>> mAllMovies;
	
	public MovieViewModel(@NonNull Context context) {
		
		this.context = context;
		this.movieList = new ArrayList<>();
		movieProgress = new ObservableInt(View.GONE);
		movieRecycler = new ObservableInt(View.GONE);
		movieLabel = new ObservableInt(View.VISIBLE);
		messageLabel = new ObservableField<>(context.getString(R.string.default_loading_movie));
		
		initializeViews();
		fetchMovieList();
	}
	
	public MovieViewModel(Application application) {
		mRepository = new MovieRepository(application);
		mAllMovies = mRepository.getAllMovies();
	}
	
	public LiveData<List<MovieObject>> getAllMovies() {
		return mAllMovies;
	}
	
	public void insert(MovieObject movie) {
		mRepository.insert(movie);
	}
	
	// It is "public" to show an example of test
	public void initializeViews() {
		movieLabel.set(View.GONE);
		movieRecycler.set(View.GONE);
		movieProgress.set(View.VISIBLE);
	}
	
	// NOTE: This method can return the observer and just subscribe to it in the activity or fragment,
	// an Activity or Fragment needn't to implement from the Observer java class
	// (To avoid RX in UI now we can use LiveData instead)
	private void fetchMovieList() {
		
		AppetiserApplication movieApplication = AppetiserApplication.create(context);
		AppetiserService movieService = movieApplication.getAppetiserService();
		
		Disposable disposable = movieService.fetchMovie(AppetiserFactory.ITUNES_URL)
		                                     .subscribeOn(movieApplication.subscribeScheduler())
		                                     .observeOn(AndroidSchedulers.mainThread())
		                                     .subscribe(new Consumer<AppetiserResponse>() {
			                                     @Override
			                                     public void accept(AppetiserResponse movieResponse) {
				                                     changeMovieDataSet(movieResponse.getMovieList());
				                                     // TODO add ROOM here
//				                                     addRecipesToDB(movieResponse.getMovieList());
				                                     movieProgress.set(View.GONE);
				                                     movieLabel.set(View.GONE);
				                                     movieRecycler.set(View.VISIBLE);
			                                     }
		                                     }, new Consumer<Throwable>() {
			                                     @Override
			                                     public void accept(Throwable throwable) {
				                                     messageLabel.set(context.getString(R.string.error_loading_movie));
				                                     movieProgress.set(View.GONE);
				                                     movieLabel.set(View.VISIBLE);
				                                     movieRecycler.set(View.GONE);
				                                     throwable.printStackTrace();
			                                     }
		                                     });
		
		compositeDisposable.add(disposable);
	}
	
	private void changeMovieDataSet(List<Movie> movies) {
		movieList.addAll(movies);
		setChanged();
		notifyObservers();
	}
	
	public List<Movie> getMovieList() {
		return movieList;
	}
	
	private void unSubscribeFromObservable() {
		if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
			compositeDisposable.dispose();
		}
	}
	
	public void reset() {
		unSubscribeFromObservable();
		compositeDisposable = null;
		context = null;
	}
}