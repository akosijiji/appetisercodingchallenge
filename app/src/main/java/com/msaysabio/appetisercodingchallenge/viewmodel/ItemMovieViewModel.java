package com.msaysabio.appetisercodingchallenge.viewmodel;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.msaysabio.appetisercodingchallenge.R;
import com.msaysabio.appetisercodingchallenge.model.Movie;
import com.msaysabio.appetisercodingchallenge.view.MovieDetailActivity;

import androidx.databinding.BaseObservable;
import androidx.databinding.BindingAdapter;

/**
 * Created by Mary.Sabio on 30-08-2019.
 */
public class ItemMovieViewModel extends BaseObservable {
	
	private Movie movie;
	private Context context;
	
	// Constructor
	public ItemMovieViewModel(Movie movie, Context context) {
		this.movie = movie;
		this.context = context;
	}
	
	// Getters
	public String getArtistName() {
		return movie.artistName;
	}
	
	public String getCollectionName() {
		return movie.collectionName;
	}
	
	public String getTrackName() {
		return movie.trackName;
	}
	
	public String getCurrency() {
		return movie.currency;
	}
	
	public String getCountry() {
		return movie.country;
	}
	
	public String getTrackPrice() {
		return movie.trackPrice;
	}
	
	public String getReleaseDate() {
		return movie.releaseDate;
	}
	
	public String getPrimaryGenreName() {
		return movie.primaryGenreName;
	}
	
	public String getLongDescription() {
		return movie.longDescription;
	}
	
	public String getPictureProfile() {
		return movie.artworkUrl100;
	}
	
	public String getPreviewUrl() {
		return movie.previewUrl;
	}
	
	// Bind adapter to ImageView
	@BindingAdapter("imageUrl")
	public static void setImageUrl(ImageView imageView, String url) {
		Glide.with(imageView.getContext())
		     .load(url)
		     .placeholder(R.drawable.ic_placeholder)
		     .into(imageView);
	}
	
	// Listener for clicking on RecyclerView which will redirect to movie details
	public void onItemClick(View view) {
		context.startActivity(MovieDetailActivity.launchDetail(view.getContext(), movie));
	}
	
	// To set movie object and notify its change(s)
	public void setMovie(Movie movie) {
		this.movie = movie;
		notifyChange();
	}
}
