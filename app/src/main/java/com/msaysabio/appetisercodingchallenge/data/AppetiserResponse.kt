package com.msaysabio.appetisercodingchallenge.data

import com.google.gson.annotations.SerializedName
import com.msaysabio.appetisercodingchallenge.model.Movie

/**
 * Created by Mary.Sabio on 30-08-2019.
 */
class AppetiserResponse {
	
	// To get service response
	@SerializedName("results")
	val movieList: List<Movie>? = null
	
}
