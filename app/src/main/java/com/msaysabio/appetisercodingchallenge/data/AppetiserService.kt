package com.msaysabio.appetisercodingchallenge.data

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Url

/**
 * Created by Mary.Sabio on 30-08-2019.
 */

// Interface to get retrieve movies
interface AppetiserService {
	
	@GET
	fun fetchMovie(@Url url: String): Observable<AppetiserResponse>
	
}
