package com.msaysabio.appetisercodingchallenge.data;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Mary.Sabio on 30-08-2019.
 */
public class AppetiserFactory {
	
	private final static String BASE_URL = "https://itunes.apple.com/";
	public final static String ITUNES_URL = "https://itunes.apple.com/search?term=star&amp;country=au&amp;media=movie&amp;all";
	
	public static AppetiserService create() {
		HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
		logging.setLevel(HttpLoggingInterceptor.Level.BODY);
		OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
		httpClient.addInterceptor(logging);
		Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
		                                          .addConverterFactory(GsonConverterFactory.create())
		                                          .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
		                                          .client(httpClient.build())
		                                          .build();
		return retrofit.create(AppetiserService.class);
	}
	
}
