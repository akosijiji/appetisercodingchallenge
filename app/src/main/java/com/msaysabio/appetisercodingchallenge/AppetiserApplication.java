package com.msaysabio.appetisercodingchallenge;

import android.content.Context;

import com.msaysabio.appetisercodingchallenge.data.AppetiserFactory;
import com.msaysabio.appetisercodingchallenge.data.AppetiserService;

import androidx.multidex.MultiDexApplication;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Mary.Sabio on 30-08-2019.
 */
public class AppetiserApplication extends MultiDexApplication {
	
	private AppetiserService peopleService;
	private Scheduler scheduler;
	
	private static AppetiserApplication get(Context context) {
		return (AppetiserApplication) context.getApplicationContext();
	}
	
	public static AppetiserApplication create(Context context) {
		return AppetiserApplication.get(context);
	}
	
	public AppetiserService getAppetiserService() {
		if (peopleService == null) {
			peopleService = AppetiserFactory.create();
		}
		return peopleService;
	}
	
	public Scheduler subscribeScheduler() {
		if (scheduler == null) {
			scheduler = Schedulers.io();
		}
		return scheduler;
	}
	
}
