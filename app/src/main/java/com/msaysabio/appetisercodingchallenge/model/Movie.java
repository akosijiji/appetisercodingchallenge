package com.msaysabio.appetisercodingchallenge.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Mary.Sabio on 30-08-2019.
 */

// POJO class for Movie
public class Movie implements Serializable {
	
	@SerializedName("trackName")
	public String trackName;
	
	@SerializedName("collectionName")
	public String collectionName;
	
	@SerializedName("artistName")
	public String artistName;
	
	@SerializedName("artworkUrl100")
	public String artworkUrl100;
	
	@SerializedName("country")
	public String country;
	
	@SerializedName("currency")
	public String currency;
	
	@SerializedName("primaryGenreName")
	public String primaryGenreName;
	
	@SerializedName("collectionPrice")
	public String collectionPrice;
	
	@SerializedName("trackPrice")
	public String trackPrice;
	
	@SerializedName("trackRentalPrice")
	public String trackRentalPrice;
	
	@SerializedName("releaseDate")
	public String releaseDate;
	
	@SerializedName("longDescription")
	public String longDescription;
	
	@SerializedName("previewUrl")
	public String previewUrl;
	
}
