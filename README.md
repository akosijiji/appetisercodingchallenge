# AppetiserCodingChallenge

Android test app of Appetiser Apps.

## Architecture

App is designed using MVVM architecture. 
I chose MVVM over MVP because MVVM combines the advantages of separation of concerns provided by MVP, and at the same time leveraging the advantages of data bindings. 
The model is the one who drives as many of the operations as possible, minimizing the logic in the view.

## Dependencies

Androidx as support library.

Retrofit as HTTP client with OKHttpClient as well.

GSON as the library to serialise and deserialise Java objects to JSON.

Glide as image loading and caching library.

RXJava as reactive extension for composing asynchronous and event-based program using observable.

Room as local persistence library.

## UI/UX Design

I follow Material Design that is the standard for Android apps.
